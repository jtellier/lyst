from enum import Enum


class Idea:
    def __init__(self, description: str):
        self.description = description
        self.done = False

    def __eq__(self, other):
        return self.description == other.description

    def finished(self):
        self.done = True


class Theme:
    def __init__(self, goal: str):
        self.goal = goal
        self.ideas = []

    def __eq__(self, other):
        return self.goal == other.goal and self.ideas == other.ideas

    def enrich(self, idea: str):
        self.ideas.append(Idea(idea))

    def finish(self, id: int):
        self.ideas[id].finished()


class Project:
    def __init__(self):
        self.themes = []
        self.current_theme = None

    def create_theme(self, goal: str):
        new_theme = Theme(goal)
        self.themes.append(new_theme)
        self.current_theme = new_theme

    def switch_to(self, id: int):
        self.current_theme = self.themes[id]


class InvalidCommandError(Exception):
    def __init__(self, message: str):
        self.message = message


class UserAction(Enum):
    ADD_IDEA = '+'
    FINISH_IDEA = '!'
    SWITCH_THEME = '?'
    CREATE_THEME = '>'


class UserCommand:
    def __init__(self, user_action: UserAction, content: str) -> None:
        self.user_action = user_action
        self.content = content
