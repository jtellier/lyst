from abc import ABC, abstractmethod

from lyst.domain import Project, UserCommand, UserAction


class ProjectLoader(ABC):
    @abstractmethod
    def read(self, uri: str) -> str:
        pass

    @abstractmethod
    def load(self, raw_project: str) -> Project:
        pass


class ProjectSaver(ABC):
    @abstractmethod
    def dump(self, project: Project) -> str:
        pass

    @abstractmethod
    def save(self, uri: str, raw_project: str) -> None:
        pass


class ProjectView(ABC):
    @abstractmethod
    def display(self, project: Project):
        pass


class ProjectController(ABC):
    @abstractmethod
    def read_command(self) -> str:
        pass

    @abstractmethod
    def parse_command(self, project: Project, command: str) -> UserCommand:
        pass

    def execute_command(self, project: Project, command: UserCommand) -> None:
        if command.user_action == UserAction.ADD_IDEA:
            project.current_theme.enrich(command.content)
        elif command.user_action == UserAction.FINISH_IDEA:
            project.current_theme.finish(int(command.content))
        elif command.user_action == UserAction.CREATE_THEME:
            project.create_theme(command.content)
        else:
            project.switch_to(int(command.content))
