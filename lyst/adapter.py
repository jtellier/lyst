import json

from lyst.domain import Project, UserCommand, UserAction, InvalidCommandError
from lyst.port import ProjectLoader, ProjectSaver, ProjectView, ProjectController


class JsonFileProjectLoader(ProjectLoader):
    def read(self, uri: str) -> str:
        with open(uri, 'r') as project:
            return project.read()

    def load(self, raw_project: str) -> Project:
        dict_project = json.loads(raw_project)
        project = Project()
        for theme in dict_project['themes']:
            project.create_theme(theme['goal'])
            for idea in theme['ideas']:
                project.current_theme.enrich(idea['description'])
                if idea['done']:
                    project.current_theme.ideas[-1].finished()
        return project


class JsonFileProjectSaver(ProjectSaver):
    def dump(self, project: Project) -> str:
        raw_project = {'themes': []}
        for theme in project.themes:
            raw_theme = {'goal': theme.goal, 'ideas': []}
            for idea in theme.ideas:
                raw_idea = {'description': idea.description, 'done': idea.done}
                raw_theme['ideas'].append(raw_idea)
            raw_project['themes'].append(raw_theme)

        return json.dumps(raw_project)

    def save(self, uri: str, raw_project: str) -> None:
        with open(uri, 'w') as project:
            project.write(raw_project)


class TerminalProjectView(ProjectView):
    def display(self, project: Project):
        printable_project = ''
        for theme in project.themes:
            bullet = '\u2217' if theme.ideas else '\u2205'
            theme_line = '%s %s' % (bullet, theme.goal)
            if theme == project.current_theme:
                printable_project += '\033[1m' + theme_line + '\033[0m' + '\n'
            else:
                printable_project += theme_line + '\n'
            for idea in theme.ideas:
                bullet = '\u2713' if idea.done else '\u22C5'
                printable_project += '\t%s %s\n' % (bullet, idea.description)
        print(printable_project)


class TerminalProjectController(ProjectController):
    def read_command(self) -> str:
        print('[>] add theme, [?] switch theme, [+] add idea, [!] finish idea')
        user_input = input('... ')
        return user_input

    def parse_command(self, project: Project, command: str) -> UserCommand:
        content = command[1:].strip()
        action_code = command[0]

        if action_code == UserAction.ADD_IDEA.value and not content:
            raise InvalidCommandError('Please provide a description')

        if action_code == UserAction.CREATE_THEME.value and not content:
            raise InvalidCommandError('Please provide a goal')

        if action_code == UserAction.SWITCH_THEME.value:
            self.validate_index(content, project.themes)

        if action_code == UserAction.FINISH_IDEA.value:
            ideas = project.current_theme.ideas
            self.validate_index(content, ideas)

        return UserCommand(UserAction(action_code), content)

    @staticmethod
    def validate_index(content, ideas):
        try:
            index = int(content)
            if index < 0 or index > len(ideas) - 1:
                raise InvalidCommandError('Please provide a valid index')
        except ValueError:
            raise InvalidCommandError('Please provide a valid index')
