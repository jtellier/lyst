# Lyst
Lyst is a TODO list management app, implemented with TDD and following a clean architecture guideline.
I ported the app to use it within a CLI with file storage, but you may as well port it to run on the web with a database storage.

## Requirements
The goal of this kata is to write a TODO list management app with the constraint of following a hexagonal / clean architecture, a.k.a. organizing your code using 3 layers :
+ the adapter layer: in charge of allowing your app to use a specific environment
+ the port layer: in charge of providing interfaces for the adapters to interact with the domain
+ the domain: in charge of implementing the use cases and the domain model

Your app should allow the user to :
+ add a theme to the project
+ add an idea to the current theme
+ tag an idea as 'done'
+ switch the current theme to another one
+ display the project tree (1 project > N themes > P ideas)
+ load a project from a data source
+ save the project to a data source

## Hexagonal / Clean Architecture

![](architecture.png)

## Install requirements
Lyst runs on Python 3.6.

```
. venv/bin/activate
pip install -r requirements.txt
```

## Unit test execution
```
. venv/bin/activate
pytest -v
```

## Test coverage
```
. venv/bin/activate
pytest test --cov=app --cov-report=html -v
```